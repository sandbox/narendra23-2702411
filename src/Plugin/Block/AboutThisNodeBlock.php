<?php

namespace Drupal\about_this_node\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityManagerInterface;

/**
 * Provides a 'Node information' block.
 *
 * @Block(
 *   id = "about_this_node_block",
 *   admin_label = @Translation("About this node block")
 * )
 */
class AboutThisNodeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;
  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;
  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructs an AboutThisNodeBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object to be checked.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The Config factory.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RouteMatchInterface $route_match, ConfigFactory $config_factory, DateFormatterInterface $date_formatter, EntityManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->routeMatch = $route_match;
    $this->configFactory = $config_factory;
    $this->dateFormatter = $date_formatter;
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $output = [];
    $node = $this->routeMatch->getParameter('node');
    if ($node) {
      if ($node->id()) {
        $node_info = $this->buildRelatedNodeInfo($node);
        $output = [
          '#theme' => 'about_this_node',
          '#node_info' => $node_info,
          '#attached' => [
            'library' => [
              'about_this_node/about_this_node.default_css',
            ],
          ],
          '#cache' => [
            'contexts' => [
              // Cacheable per URL.
              'url',
            ],
          ],
        ];
      }
    }
    return $output;
  }

  /**
   * Gather information about the node and return it as a keyed array.
   */
  public function buildRelatedNodeInfo($node) {
    $node_info = [];

    // Node ID.
    $node_info['node_id'] = [
      'label' => $this->t('Node ID (NID):'),
      'value' => $node->id(),
    ];

    // Node type.
    $node_info['node_type'] = [
      'label' => $this->t('Node type:'),
      'value' => $node->getType(),
    ];

    // Gather author info.
    $author_uid = $node->getOwnerId();

    $author = '';
    $author_output = '';
    // Check if author is anonymous.
    if ($author_uid == '0') {
      $author_output = $this->configFactory->get('user.settings')->get('anonymous');
    }
    else {
      $author = $this->entityManager->getStorage('user')->load($author_uid);
      $author_username = $author->getAccountName();
      $url = Url::fromRoute('entity.user.canonical', ['user' => $author_uid]);
      $author_output = Link::fromTextAndUrl($author_username, $url);
    }

    // Author name/time.
    $node_info['created'] = [
      'label' => $this->t('Created on:'),
      'value' => [
        'created_on' => [
          'label' => $this->t('on'),
          'value' => $this->dateFormatter->format($node->getCreatedTime(), 'short'),
        ],
        'created_by' => [
          'label' => $this->t('Created by'),
          'value' => $author_output,
        ],
      ],
    ];

    // Gather last updated author info.
    $update_author_uid = $node->getRevisionAuthor()->id();
    $update_author = '';
    $update_author_output = '';
    // Check if author is anonymous.
    if ($update_author_uid == '0') {
      $update_author_output = $this->configFactory->get('user.settings')->get('anonymous');
    }
    else {
      $update_author = $this->entityManager->getStorage('user')->load($update_author_uid);
      $update_author_username = $update_author->getAccountName();
      $url = Url::fromRoute('entity.user.canonical', ['user' => $update_author_uid]);
      $update_author_output = Link::fromTextAndUrl($update_author_username, $url);
    }
    // Last updated author name/time.
    $node_info['updated'] = [
      'label' => $this->t('Last updated:'),
    ];

    // Check if node has never been updated.
    if ($node->getCreatedTime() == $node->getChangedTime()) {
      $node_info['updated']['value'] = $this->t('Never');
    }
    else {
      $node_info['updated']['value'] = [
        'updated_on' => [
          'label' => $this->t('on'),
          'value' => $this->dateFormatter->format($node->getChangedTime(), 'short'),
        ],
        'updated_by' => [
          'label' => $this->t('Updated by'),
          'value' => $update_author_output,
        ],
      ];
    }

    // Published status.
    $node_info['published'] = [
      'label' => $this->t('Published:'),
    ];
    $node_info['published']['value'] = ($node->isPublished() == 1) ? $this->t('Yes') : $this->t('No');

    // Promoted to front page status.
    $node_info['promoted'] = [
      'label' => $this->t('Promoted:'),
    ];
    $node_info['promoted']['value'] = ($node->isPromoted() == 1) ? $this->t('Yes') : $this->t('No');

    // Sticky status.
    $node_info['stickied'] = [
      'label' => $this->t('Stickied:'),
    ];
    $node_info['stickied']['value'] = ($node->isSticky() == 1) ? $this->t('Yes') : $this->t('No');

    // Commenting status.
    $node_info['commenting'] = [
      'label' => $this->t('Commenting:'),
    ];

    $comments = comment_node_search_result($node);
    $comment_value = ($comments) ? $comments['comment'] : $this->t('Comments not open');

    $node_info['commenting']['value'] = $comment_value;
    return $node_info;
  }

}
