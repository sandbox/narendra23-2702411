=====
D8 About This Node
https://www.drupal.org/sandbox/narendra23/2702411
This is a drupal 8 version of https://www.drupal.org/project/about_this_node
-----

About This Node creates a block that displays information about
the node you're viewing. It allows users to see, at a glance,
the following information without clicking "Edit" or digging
into revision history:

    * node ID (NID)
    * node type (content type)
    * creation date and time
    * creation author (user who created the node)
    * last updated date and time
    * last updated author (if revision is set)
    * published status
    * promoted to front page status
    * sticky status
    * commenting status

This module, when enabled, creates a block that displays the
information about a node. You will need to tell the module the
content types for which it should display the block and display
this block in one of your theme's regions.
